class PluginLoader {
    loaded(client) {
        require('./Plugins/ServerStatus')(client);
    }

    message(message) {
        require('./Plugins/HelpTickets')(message);
    }

    guildMemberAdd(member) {
        require('./Plugins/Confirm/MemberAdd')(member);
    }

    guildMemberRemove(member) {
        require('./Plugins/Confirm/MemberRemove')(member);
    }
}

module.exports = new PluginLoader;
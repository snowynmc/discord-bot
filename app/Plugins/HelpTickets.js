module.exports = function (message) {
    if (message.channel.name == 'help-requests') {
        message.delete();
        if (message.content.startsWith('~') == false) {
            message.reply('You can only create help tickets in this channel.').then(function (sent) {
                sent.delete(4000);
            });
        }
    }
};
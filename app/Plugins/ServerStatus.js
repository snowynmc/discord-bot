const axios = require('axios');

function checkStatus(client) {
    axios.get('http://docker.snowynmc.ga/shared/online').then(function (resp) {
        client.user.setActivity(`With ${resp.data.online} Players`);
    });
}

module.exports = function (client) {
    setInterval(checkStatus, 60000, client);
};
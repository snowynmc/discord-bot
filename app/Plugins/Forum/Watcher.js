const Discord = require('discord.js');
const Watcher = require('./Watcher');

module.exports = function (client) {
    marketWatcher(client);
    updateWatcher(client);
};

function marketWatcher(client) {
    const channel = client.channels.get('495528576217513984');
    const watcher = new Watcher('https://forum.snowynmc.ga/api/index.php?threads&forum_id=8');
    watcher.on('newentries', function (data) {
        data.threads.forEach(function (item) {
            const embed = new Discord.RichEmbed();
            embed.setTitle(item.thread_title);
            embed.setAuthor(item.creator_username, item.links.first_poster_avatar);
            embed.setDescription('Check out this post in the marketplace to see whats being offered.');
            embed.setURL(item.links.permalink);
            embed.setColor(0x93B48B);
            channel.send(embed);
        });
    });
    watcher.start();
}

function updateWatcher(client) {
    const channel = client.channels.get('486229012734279700');
    const watcher = new Watcher('https://forum.snowynmc.ga/api/index.php?threads&forum_id=4');
    watcher.on('newentries', function (data) {
        data.threads.forEach(function (item) {
            const embed = new Discord.RichEmbed();
            embed.setTitle(item.thread_title);
            embed.setAuthor(item.creator_username, item.links.first_poster_avatar);
            embed.setDescription(item.first_post.post_body_plain_text.substring(0, 276).trim());
            embed.setURL(item.links.permalink);
            embed.setColor(0x93B48B);
            channel.send(embed);
        });
    });
    watcher.start();
}
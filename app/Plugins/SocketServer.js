const Env = use('Env');
const net = require('net');
const os = require('os');

module.exports = function (client) {
    const server = new net.Socket();
    server.connect(4080, Env.get('SOCKET_HOST'));
    client.on('message', function (message) {
        if (message.channel.id == '486229012734279700') {
            if (message.author.id == '469666648052203520') return;
            emit(server, 'discord-announcement', null);
        }
    });
    server.on('close', function () {
        server.setTimeout(4000, function () {
            server.connect(4080, Env.get('SOCKET_HOST'));
        });
    });
    server.on('error', console.error);
};

function emit(client, event, data) {
    const parsed = JSON.stringify({
        namespace: 'customs',
        event: event,
        data: data
    });
    client.write(`4::${parsed}${os.EOL}`);
}
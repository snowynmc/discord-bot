const Whitelist = use('App/Helpers/Whitelist');
const ConfirmName = use('App/Models/ConfirmName');

module.exports = function (member) {
    const model = ConfirmName.query().where('discord_user_id', member.user.id);
    model.first().then(function (user) {
        user.delete();
        Whitelist.remove(user.minecraft_name);
    });
};
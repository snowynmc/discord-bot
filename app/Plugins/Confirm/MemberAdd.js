const Helper = use('App/Helpers/Discord');
const ConfirmName = use('App/Models/ConfirmName');

module.exports = function (member) {
    const model = ConfirmName.query().where('discord_user_id', member.user.id);
    if (model.getCount() == 0) {
        const channel = Helper.find(member.guild.channels, 'id', 520307382652502036);
        channel.send(`Welcome **${member}** please confirm your minecraft name using the command **~confirm-mc**`);
    }
};
const BaseCommand = use('App/Commands/BaseCommand');
const Whitelist = use('App/Helpers/Whitelist');
const ConfirmName = use('App/Models/ConfirmName');

module.exports = new BaseCommand({
    name: 'sync-confirm',
    usage: 'sync-confirm',
    description: 'Synchronize confirmed users with the whitelist.',
    removeUserMessage: true,
    roles: new Array('Admin', 'Helper'),
    resolve: function (message) {
        const model = ConfirmName.query().whereNotNull('minecraft_name');
        model.fetch().then(function (users) {
            users.forEach(function (item) {
                Whitelist.add(item.minecraft_name);
            });
            message.reply(`${rows.length} users have been synced with the whitelist.`);
        });
    }
});
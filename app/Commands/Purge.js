const BaseCommand = use('App/Commands/BaseCommand');
const dash = require('lodash');

module.exports = new BaseCommand({
    name: 'purge',
    usage: 'purge [amount]',
    description: 'Purge messages from the channel.',
    removeUserMessage: true,
    roles: new Array('Admin'),
    resolve: function (message, args) {
        message.channel.bulkDelete(dash.head(args)).catch(function (error) {
            console.log('Could not fetch more messages to purge.', error.message);
        });
    }
});
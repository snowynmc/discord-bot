const BaseCommand = use('App/Commands/BaseCommand');
const ConfirmName = use('App/Models/ConfirmName');

module.exports = new BaseCommand({
    name: 'confirm-mc',
    usage: 'confirm-mc',
    description: 'Creates a confirmation code for the user.',
    resolve: function (message) {
        if (message.channel.name == 'confirm-minecraft-name') {
            message.delete();
            const member = message.member;
            const user = new ConfirmName();
            user.discord_user_id = member.user.id;
            user.discord_user_name = member.user.username;
            user.discord_user_tag = member.user.discriminator;
            user.save().then(function () {
                message.reply(`Join our hub server with **play.snowynmc.ga** and enter the command **discord ${member.user.id}**`);
            }).catch(function (error) {
                console.log('Could not create discord user to confirm.', error.message);
            });
        }
    }
});
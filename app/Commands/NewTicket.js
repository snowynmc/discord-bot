const BaseCommand = use('App/Commands/BaseCommand');

module.exports = new BaseCommand({
    name: 'new-ticket',
    usage: 'new-ticket',
    description: 'Creates a new help ticket.',
    resolve: function (message) {
        if (message.channel.name == 'help-requests') {
            const channel = `ticket-${message.author.id}`;
            const permissions = new Array('SEND_MESSAGES', 'READ_MESSAGES');
            const allowed = new Array('486228284724477952', '491074753143111710');
            const overwrites = new Array;
            overwrites.push({
                id: message.author.id,
                allow: permissions
            });
            overwrites.push({
                id: '486227671311974411',
                deny: permissions
            });
            allowed.forEach(function (role) {
                overwrites.push({
                    id: role,
                    allow: permissions
                });
            });
            message.guild.createChannel(channel, {
                type: 'text',
                permissionOverwrites: overwrites
            }).then(function (item) {
                item.send('Please explain with as much detail as possible why this ticket was opened.');
            }).catch(function (error) {
                console.log('Could not create the ticket channel.', error.message);
            });
        }
    }
});
const BaseCommand = use('App/Commands/BaseCommand');
const Helper = use('App/Helpers/Discord');
const Discord = require('discord.js');
const dash = require('lodash');

module.exports = new BaseCommand({
    name: 'user-info',
    usage: 'user-info [user]',
    description: 'Gets information about a user.',
    removeUserMessage: false,
    roles: new Array('Admin'),
    resolve: function (message, args) {
        const name = dash.head(args);
        const member = Helper.find(message.guild.members, 'displayName', name);
        if (member == null) {
            return message.reply('Sorry we could not find that user.');
        }
        const roles = member.roles.map(item => item.name).join(' - ');
        const gamePlaying = member.presence.game ? member.presence.game.name : 'Not Playing Game';
        const embed = new Discord.RichEmbed();
        embed.setTitle(member.displayName);
        embed.addField('Roles', roles);
        embed.addField('Joined', member.joinedAt);
        embed.addField('Created', member.user.createdAt);
        embed.addField('Status', member.presence.status);
        embed.addField('Game', gamePlaying);
        message.channel.send({
            embed: embed
        });
    }
});
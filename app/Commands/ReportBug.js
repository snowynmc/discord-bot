const BaseCommand = use('App/Commands/BaseCommand');
const Env = use('Env');
const axios = require('axios');
const parseArgs = require('yargs-parser');

module.exports = new BaseCommand({
    name: 'new-issue',
    usage: 'new-issue --s=server message',
    description: 'Creates a new issues on the bug tracker.',
    roles: new Array('Admin', 'Helper'),
    resolve: function (message, args) {
        if (message.channel.name == 'staff-room') {
            message.delete();
            const params = parseArgs(args);
            const servers = new Array('Hub', 'Vanilla', 'SkyBlock', 'Network', 'Modded');
            if (servers.includes(params.s) == false) {
                message.reply('You entered an invalid server name.').then(sent => sent.delete(4000));
                return false;
            }
            axios.request({
                method: 'post',
                url: 'https://gitlab.com/api/v4/projects/12915769/issues',
                data: {
                    private_token: Env.get('GITLAB_TOKEN'),
                    title: params._.join(' '),
                    description: `Created by ${message.member.nickname}`,
                    labels: `Server - ${params.s}`
                }
            }).then(function (resp) {
                message.reply(`You have created issue **${resp.data.iid}** on the tracker.`);
            }).catch(console.error);
        }
    }
});
const BaseCommand = use('App/Commands/BaseCommand');
const Discord = require('discord.js');

module.exports = function (commands) {
    const embed = new Discord.RichEmbed();
    embed.setTitle('SnowynMC Bot Commands');
    Object.values(commands).forEach(function (item) {
        embed.addField(item.usage, item.description);
    });
    return new BaseCommand({
        name: 'help',
        usage: 'help',
        description: 'Shows the help menu.',
        resolve: function (message) {
            message.channel.send({
                embed: embed
            });
        }
    });
};
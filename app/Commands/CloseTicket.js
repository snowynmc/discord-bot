const BaseCommand = use('App/Commands/BaseCommand');
const Helper = use('App/Helpers/Discord');

module.exports = new BaseCommand({
    name: 'close-ticket',
    usage: 'close-ticket',
    description: 'Closes an active help ticket.',
    resolve: function (message) {
        if (message.channel.name.startsWith('ticket')) {
            const channel = `ticket-${message.author.id}`;
            const roles = message.member.roles;
            if (message.channel.name == channel) {
                message.channel.delete();
            } else if (Helper.find(roles, 'name', 'Admin')) {
                message.channel.delete();
            }
        }
    }
});
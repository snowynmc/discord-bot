const BaseCommand = use('App/Commands/BaseCommand');
const dash = require('lodash');

module.exports = new BaseCommand({
    name: 'purge-user',
    usage: 'purge-user [user] [amount]',
    description: 'Purge users messages from the channel.',
    removeUserMessage: true,
    roles: new Array('Admin'),
    resolve: function (message, args) {
        message.channel.fetchMessages({
            limit: dash.get(args, 1)
        }).then(function (messages) {
            messages.filter(function (item) {
                return item.author.id == dash.get(args, 0);
            }).forEach(function (item) {
                item.delete();
            });
        }).catch(function (error) {
            console.log('Could not fetch more messages to purge.', error.message);
        });
    }
});
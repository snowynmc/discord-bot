const Helper = use('App/Helpers/Discord');
const Env = use('Env');

class Command {
    constructor(options) {
        this.name = options.name;
        this.usage = options.usage;
        this.description = options.description;
        this.removeUserMessage = options.removeUserMessage;
        this.roles = options.roles;
        this.resolve = options.resolve;
        this.prefix = Env.get('BOT_MESSAGE_PREFIX');
    }

    process(message) {
        const content = message.content.trim();
        const args = content.replace(this.prefix, '').split(' ');
        if (this.hasRoles(message) == false) {
            message.reply('You do not have permission for that command.');
        } else {
            this.resolve(message, args.slice(1));
        }
        if (this.removeUserMessage) {
            message.delete(4000);
        }
    }

    hasRoles(message) {
        if (this.roles == undefined) return true;
        return this.roles.map(function (item) {
            return Helper.find(message.guild.roles, 'name', item);
        }).filter(function (role) {
            return message.member.roles.has(role.id);
        }).length > 0;
    }
}

module.exports = Command;
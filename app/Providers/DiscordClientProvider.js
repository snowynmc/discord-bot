const {ServiceProvider} = require('@adonisjs/fold');
const Discord = require('discord.js');

class DiscordClientProvider extends ServiceProvider {
    register() {
        this.registerCommandHandler();
        this.registerPluginLoader();
        this.registerDiscordClient();
        this.loginDiscordClient();
    }

    registerCommandHandler() {
        this.app.singleton('DiscordCommands', function () {
            return require('../CommandLoader');
        });
    }

    registerPluginLoader() {
        this.app.singleton('DiscordPlugins', function () {
            return require('../PluginLoader');
        });
    }

    registerDiscordClient() {
        this.app.singleton('DiscordClient', function () {
            return new Discord.Client();
        });
    }

    loginDiscordClient() {
        const sm = this;
        const env = this.app.use('Env');
        const logger = this.app.use('Logger');
        const client = this.app.use('DiscordClient');
        client.login(env.get('DISCORD_TOKEN'));
        client.on('ready', function () {
            logger.info('The discord bot has started.');
            sm.app.use('DiscordPlugins').loaded(client);
        });
        client.on('message', function (message) {
            if (message.author.bot) return;
            sm.app.use('DiscordCommands').handle(message);
            sm.app.use('DiscordPlugins').message(message);
        });
        client.on('guildMemberAdd', function (member) {
            sm.app.use('DiscordPlugins').guildMemberAdd(member);
        });
        client.on('guildMemberRemove', function (member) {
            sm.app.use('DiscordPlugins').guildMemberRemove(member);
        });
        client.on('error', console.error);
    }

    boot() {
        require('../Helpers/StringFormat');
    }
}

module.exports = DiscordClientProvider;
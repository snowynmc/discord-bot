const Env = use('Env');
const dash = require('lodash');

class CommandLoader {
    constructor() {
        this.commands = {};
        this.load();
    }

    register(command) {
        dash.set(this.commands, command.name, command);
    }

    load() {
        this.register(require('./Commands/Purge'));
        this.register(require('./Commands/PurgeUser'));
        this.register(require('./Commands/UserDetails'));
        this.register(require('./Commands/ConfirmSync'));
        this.register(require('./Commands/NewTicket'));
        this.register(require('./Commands/CloseTicket'));
        this.register(require('./Commands/Confirm'));
        this.register(require('./Commands/ReportBug'));
        this.register(require('./Commands/Help')(this.commands));
    }

    handle(message) {
        const content = message.content.trim();
        const prefix = Env.get('BOT_MESSAGE_PREFIX');
        if (content.startsWith(prefix)) {
            const args = content.replace(prefix, '').split(' ');
            const name = dash.head(args).toLowerCase();
            if (dash.has(this.commands, name) == false) {
                message.reply('Sorry that command does not exist.').then(function (sent) {
                    sent.delete(4000);
                }).catch(console.error);
            } else {
                dash.get(this.commands, name).process(message);
            }
        }
    }
}

module.exports = new CommandLoader;
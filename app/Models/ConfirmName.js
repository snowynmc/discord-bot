const Model = use('Model');

class ConfirmName extends Model {
    static get table() {
        return 'discord_users';
    }

    static get primaryKey() {
        return 'discord_user_id';
    }

    static get incrementing() {
        return false;
    }
}

module.exports = ConfirmName;
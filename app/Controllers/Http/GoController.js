const Helper = use('App/Helpers/Discord');
const Whitelist = use('App/Helpers/Whitelist');
const Response = use('App/Helpers/Response');
const ConfirmName = use('App/Models/ConfirmName');
const Client = use('DiscordClient');
const Env = use('Env');

class GoController {
    go(ctx) {
        return ctx.response.redirect('https://discordapp.com/invite/Sge5dr6');
    }

    confirm(ctx) {
        const request = ctx.request;
        const code = request.input('code');
        const model = ConfirmName.query().where('discord_user_id', code);
        model.first().then(function (user) {
            const guild = Helper.find(Client.guilds, 'id', Env.get('GUILD_ID'));
            const member = Helper.find(guild.members, 'id', user.discord_user_id);
            user.minecraft_name = request.input('name');
            user.minecraft_id = request.input('id');
            user.save().then(function () {
                member.setNickname(user.minecraft_name).catch(console.error);
                member.addRole(Helper.find(guild.roles, 'id', 520306181340921876));
                Whitelist.add(user.minecraft_name);
            });
        }).catch(function (error) {
            console.log('Could not find discord user to reconfirm.', error.message);
        });
        Response.success(ctx, null);
    }

    reconfirm(ctx) {
        const request = ctx.request;
        const name = request.input('id');
        const model = ConfirmName.query().where('minecraft_id', name);
        model.first().then(function (user) {
            const guild = Helper.find(Client.guilds, 'id', Env.get('GUILD_ID'));
            const member = Helper.find(guild.members, 'id', user.discord_user_id);
            user.minecraft_name = request.input('name');
            user.save().then(function () {
                member.setNickname(user.minecraft_name).catch(console.error);
            });
        }).catch(function (error) {
            console.log('Could not find discord user to reconfirm.', error.message);
        });
        Response.success(ctx, null);
    }
}

module.exports = GoController;
const Env = use('Env');
const axios = require('axios');

class Whitelist {
    static add(name) {
        axios.post('http://docker.snowynmc.ga/whitelist/add', {
            token: Env.get('DOCKER_TOKEN'),
            servers: new Array('mech-magic'),
            name: name
        });
    }

    static remove(name) {
        axios.post('http://docker.snowynmc.ga/whitelist/remove', {
            token: Env.get('DOCKER_TOKEN'),
            servers: new Array('mech-magic'),
            name: name
        });
    }
}

module.exports = Whitelist;
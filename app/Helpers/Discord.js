const dash = require('lodash');

class Discord {
    static find(items, name, value) {
        return items.find(function (item) {
            return dash.get(item, name) == value;
        });
    }
}

module.exports = Discord;
const util = require('util');

String.prototype.format = function () {
    const args = Array.prototype.slice.call(arguments);
    args.unshift(this.toString());
    return util.format.apply(util, args);
};
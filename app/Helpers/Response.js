const dash = require('lodash');

class Response {
    static success(ctx, data) {
        data = dash.defaults(data, {
            status: true
        });
        return ctx.response.ok(data);
    }
}

module.exports = Response;
const Route = use('Route');

Route.get('', 'GoController.go');
Route.post('confirm', 'GoController.confirm');
Route.post('reconfirm', 'GoController.reconfirm');
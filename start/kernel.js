const Server = use('Server');

const globalMiddleware = [
    'Adonis/Middleware/BodyParser',
    'Adonis/Middleware/Session',
    'Adonis/Middleware/Shield'
];

const serverMiddleware = [
    'Adonis/Middleware/Static',
    'Adonis/Middleware/Cors'
];

Server.registerGlobal(globalMiddleware).use(serverMiddleware);
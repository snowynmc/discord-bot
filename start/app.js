const path = require('path');

function customProvider(name) {
    return path.join(__dirname, '..', 'app', name);
}

const providers = [
    '@adonisjs/framework/providers/AppProvider',
    '@adonisjs/framework/providers/ViewProvider',
    '@adonisjs/lucid/providers/LucidProvider',
    '@adonisjs/bodyparser/providers/BodyParserProvider',
    '@adonisjs/cors/providers/CorsProvider',
    '@adonisjs/shield/providers/ShieldProvider',
    '@adonisjs/session/providers/SessionProvider',
    customProvider('Providers/DiscordClientProvider')
];

const aceProviders = [
    '@adonisjs/lucid/providers/MigrationsProvider'
];

const aliases = {};

const commands = [];

module.exports = {providers, aceProviders, aliases, commands};
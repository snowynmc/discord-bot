module.exports = {
    csp: {
        directives: {},
        reportOnly: false,
        setAllHeaders: false,
        disableAndroid: true
    },
    xss: {
        enabled: true,
        enableOnOldIE: false
    },
    xframe: 'DENY',
    nosniff: false,
    noopen: true,
    csrf: {
        enable: false,
        methods: new Array('POST', 'PUT', 'DELETE'),
        filterUris: new Array,
        cookieOptions: {
            httpOnly: false,
            sameSite: true,
            path: '/',
            maxAge: 7200
        }
    }
};
module.exports = {
    json: {
        limit: '5mb',
        strict: true,
        types: new Array('application/json', 'application/csp-report')
    },
    raw: {
        types: new Array('text/plain')
    },
    form: {
        types: new Array('application/x-www-form-urlencoded')
    },
    files: {
        types: new Array('multipart/form-data'),
        maxSize: '20mb',
        autoProcess: true,
        processManually: new Array
    }
};
module.exports = {
    origin: true,
    methods: new Array('GET', 'PUT', 'PATCH', 'POST', 'DELETE'),
    headers: true,
    exposeHeaders: false,
    credentials: false,
    maxAge: 90
};